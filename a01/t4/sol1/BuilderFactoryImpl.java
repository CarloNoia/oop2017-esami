package ex2016.a01.t4.sol1;

import java.util.*;


public class BuilderFactoryImpl implements BuilderFactory {

    @Override
    public <X> ListBuilder<X> makeBasicBuilder() {
        return new BasicBuilder<>();
    }

    @Override
    public <X> ListBuilder<X> makeBuilderWithSize(int size) {
        return new BuilderWithSize<>(this.makeBasicBuilder(), size);
    }

    @Override
    public <X> ListBuilder<X> makeBuilderWithoutElements(Collection<X> from) {
        return new BuilderWithoutElements<>(this.makeBasicBuilder(), from);
    }
    
    public <X> ListBuilder<X> makeBuilderWithoutElementsAndWithSize(Collection<X> from, int size){
        return new BuilderWithoutElements<>(this.makeBuilderWithSize(size), from);
    }

}
