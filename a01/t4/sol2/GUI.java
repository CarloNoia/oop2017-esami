package ex2016.a01.t4.sol2;

import javax.swing.*;
import java.util.*;
import java.util.stream.*;


public class GUI {
    
    private List<JButton> buttonList;
    private Model model;
    private String fileName; 
    
    public GUI(String fileName, int size) throws Exception {
        this.fileName = fileName;
        JFrame jf = new JFrame();
        this.buttonList = IntStream.range(0, size).mapToObj(i->"").map(JButton::new).collect(Collectors.toList());
        buttonList.forEach(jb -> jb.addActionListener(e -> setButtons()));
        JButton jbReset = new JButton("Reset");
        jbReset.addActionListener(e -> reset());
        JPanel jp = new JPanel();
        buttonList.forEach(jp::add);
        jp.add(jbReset);
        jf.getContentPane().add(jp);
        jf.setSize(700,100);
        jf.setVisible(true);
        this.reset();
    }
    
    private void setButtons(){
        if (!this.model.hasNext()){
            this.buttonList.forEach(jb->jb.setEnabled(false));
        } else {
            int pos = this.model.next();
            this.buttonList.forEach(jb->jb.setText(String.valueOf(pos)));
        }
    }
    
    private void reset(){
        this.buttonList.forEach(jb->jb.setEnabled(true));
        this.model = new ModelImpl(fileName);
        this.setButtons();
    }
    
    public static void main(String[] s) throws Exception {
        new GUI("b.txt",10);
    }
}
