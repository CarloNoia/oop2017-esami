package ex2016.a01.t2.sol1;

import java.util.*;

public class VectorImpl<X> implements Vector<X> {
    
    private final List<X> list;
    
    VectorImpl(List<X> list){
        this.list = Collections.unmodifiableList(list);
    }

    @Override
    public Optional<X> getAtPosition(int position) {
        return position >=0 && position < this.size() ? Optional.of(this.list.get(position)) : Optional.empty();
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public List<X> asList(){
        return this.list;
    }
    
    @Override
    public void executeOnAllElements(Executor<X> executor){
        this.list.forEach(executor::execute);
    }
}
